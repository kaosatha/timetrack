#!/usr/bin/env runhaskell

--
-- PARSER FOR TIMETRACK
--
-- GABRIEL RODGERS // KAOSATHA
--
-- http://book.realworldhaskell.org/read/using-parsec.html
--
-- BSD 2-Clause
--


module Parser (
    tokenise,
    ) where

import Text.ParserCombinators.Parsec

ttCommand = endBy line eol
line = sepBy ttToken (char ' ')
ttToken = quotedToken <|> many (noneOf " \n\r")

quotedToken = do
    char '"'
    tok <- many quotedChar
    char '"' <?> "quote at end of cell"
    return tok

quotedChar = noneOf "\"" <|> try (string "\"\"" >> return '"')

eol = try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <|> string "\0"
    <|> string ";"
    <?> "end of line"

-- input MUST end with "\n"
tokenise :: String -> Either ParseError [[String]]
tokenise input = parse ttCommand "(unknown)" input


