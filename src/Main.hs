#!/usr/bin/env runhaskell

--
-- TIMETRACK
--
-- GABRIEL RODGERS // KAOSATHA
--
-- BSD 2-Clause
--

--fix double event single time slot prevention
--better parsing

module Main (main) where

import System.Directory (XdgDirectory(..), createDirectoryIfMissing,
    getXdgDirectory)
import System.Environment (getArgs)
import System.IO (FilePath)
import Control.Monad (forever)
import Data.Char (toUpper)
import Data.Maybe (fromJust, fromMaybe, isJust)
import Data.Time

import Event
import Parser

main :: IO ()
main = do
    argv <- getArgs
    let commands = map parseCommand [argv]
    mapM_ executeCommand commands

--
-- The following code deals with commands executing commands
--

data Command = Command {
    op :: Maybe Mode,
    file :: Maybe FilePath,
    start :: Maybe UTCTime,
    len :: Maybe NominalDiffTime,
    end :: Maybe UTCTime,
    etype :: Maybe String,
    subject :: Maybe String,
    place :: Maybe String,
    persons :: Maybe [String],
    message :: Maybe String,
    productive :: Maybe String
} deriving (Show)

data Mode = Add | Create | Delete | Mark | Print | Printall | Repl | Sort
    deriving (Show, Read)

blankCommand :: Command
blankCommand = Command Nothing Nothing Nothing Nothing Nothing Nothing Nothing
    Nothing Nothing Nothing Nothing

parseCommand :: [String] -> Command
parseCommand strs = pc strs $ blankCommand

pc :: [String] -> Command -> Command
pc [] c = c
pc ("file":xs:ys) c = pc ys (c {file = (Just xs)})
pc ("start":xs:ys) c = pc ys (c {start = Just (read xs :: UTCTime)})
--pc ("length":xs:ys) c = pc ys (c {len = Just (read xs)})
pc ("end":xs:ys) c = pc ys (c {end = Just (read xs :: UTCTime)})
pc ("mark":xs:ys) c = pc ys (c {op = Just Mark, productive = Just xs})
pc ("message":xs:ys) c = pc ys (c {message = Just xs})
pc ("etype":xs:ys) c = pc ys (c {etype = Just xs})
pc ("subject":xs:ys) c = pc ys (c {subject = Just xs})
pc ("place":xs:ys) c = pc ys (c {place = Just xs})
pc ("person":xs:ys) c =
    pc ys (c {persons = Just (xs:(fromMaybe [] $ persons c))})
pc (x:ys) c
    | x `elem` ["add", "create", "print", "printall", "delete", "repl", "sort"]
        = pc (ys) (c {op = Just $ read $ (toUpper $ head x):(drop 1 x)})
    | otherwise = error "bad option"

executeCommand :: Command -> IO ()
executeCommand c = do
    confDir <- getXdgDirectory XdgConfig "timetrack/"
    let file' = confDir ++ (fromMaybe "default" (file c))
        startTime = fromJust $ start c
    if isJust (op c) then case (fromJust (op c)) of
        Add -> do
            let time' = Slot (startTime) (fromJust $ end c)
                etype' = fromMaybe "No event" $ etype c
                subject' = fromMaybe "No subject" $ subject c
                place' = fromMaybe "No place" $ place c
                persons' = fromMaybe ["Nobody"] $ persons c
                e = newEvent time' etype' subject' place' persons'
            if startTime < (fromJust $ end c)
                then do
                    e' <- (getEvent file' startTime)
                    if isJust e'
                        then error "time already assigned"
                        else writeEvent file' e
                else error "end time cannot be before start time"
        Create ->
            createDirectoryIfMissing False confDir >> createTimeTable file'
        Delete -> deleteEvent file' startTime
        Mark -> markProductive file' startTime p msg
              where
                p = case (fromMaybe "N" (productive c)) of
                    "p" -> WasProductive
                    "n" -> NotProductive
                    _ -> Neutral
                msg = fromMaybe "" $ message c
        Print ->
            (getEvent file' startTime) >>= (putStrLn . prettyShow . fromJust)
        Printall -> (getEvents file') >>= (mapM_ (putStrLn . prettyShow))
        Repl -> forever $ do
            --add newline to work around quirk in tokenise
            l <- (\k -> (++) k "\n") <$> getLine
            case tokenise (l) of
                Right a -> executeCommand $ parseCommand $ head a
                Left a -> error $ "bad input\n" ++ show a
        Sort ->
            (readFile file') >>=
                (rewriteEvents file' . sortEvents . map read . lines)
    else
        error "Bad mode given - mode is Nothing"

