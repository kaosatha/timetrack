#!/usr/bin/env runhaskell

--
-- DATA.HS - Data types for timetrack
--
-- GABRIEL RODGERS // KAOSATHA
--
-- BSD 2-Clause
--

module Event (
    Event,
    Slot(Slot),
    Trool(..),
    cleanFile,
    createTimeTable,
    deleteEvent,
    getEvent,
    getEvents,
    markProductive,
    newEvent,
    newTime,
    prettyShow,
    sortEvents,
    rewriteEvents,
    writeEvent
) where


import Data.List (sortBy, intersperse)
import Data.Maybe (fromJust, isNothing)
import Data.Time
import System.Directory
import System.IO

type People = [String]

data Event = Event {
    slot :: Slot,
    details :: Details,
    productive :: Productive
} deriving (Eq, Show, Read)

data Slot = Slot {
    start :: UTCTime,
    end :: UTCTime
} deriving (Eq, Show, Read)

data Details = Details {
    eventType :: String,
    subject :: String,
    place :: String,
    people :: People
} deriving (Eq, Show, Read)

data Trool = NotProductive | Neutral | WasProductive
    deriving (Eq, Ord, Show, Read, Bounded, Enum)

data Productive = Productive {
    productive' :: Trool,
    message :: String
} deriving (Eq, Show, Read)


class (Show a) => PrettyShow a where
    prettyShow :: a -> String
    prettyShow = show
instance PrettyShow UTCTime where
    prettyShow = show
instance PrettyShow Slot where
    prettyShow (Slot s t) = prettyShow s ++ " - " ++ prettyShow t
instance PrettyShow Details where
    prettyShow (Details t s pl pe) = init' ++ f pe
      where
        init' = t ++ ": " ++ s ++ " in " ++ pl ++" with "
        f x = concatMap (\k -> k ++ (if k == last x then "" else ", ")) x
instance PrettyShow Productive where
    prettyShow (Productive Neutral _) = "No productivity assigned"
    prettyShow (Productive WasProductive msg) = "Event was productive: " ++ msg
    prettyShow (Productive NotProductive msg) = "Event was not productive: " ++ msg
instance PrettyShow Event where
    prettyShow e = s ++ d ++ p
        where f fx = (prettyShow $ fx e) ++ "\n"
              s = f slot
              d = f details
              p = f productive


newEvent :: Slot  -> String -> String -> String -> People -> Event
newEvent timeslot etype subject place people =
    let d = Details etype subject place people
        p = Productive Neutral ""
    in Event timeslot d p

writeEvent :: FilePath -> Event -> IO ()
writeEvent fname e = do
    withFile (fname) AppendMode (\handle -> do
        hPutStrLn handle $ show e)

rewriteEvents :: FilePath -> [Event] -> IO ()
rewriteEvents fname es = do
    withFile (fname ++ ".tmp") WriteMode (\handle -> do
        hPutStr handle $ unlines $ show <$> es)
    renameFile (fname ++ ".tmp") fname


cleanFile :: FilePath -> IO ()
cleanFile fname = do
    conts <- readFile fname
    withFile (fname ++ ".tmp") WriteMode (\handle -> do
        (hPutStr handle . unlines . filter (\k -> k /= "\n") . lines) conts)
    renameFile (fname ++ ".tmp") fname


createTimeTable :: FilePath -> IO ()
createTimeTable fname = do
    fexists <- doesFileExist fname
    if fexists then return () else writeFile fname ""

deleteEvent :: FilePath -> UTCTime -> IO ()
deleteEvent fname t = do
    let filtr c = unlines $ filter (\k -> not (t `timeIsIn` (read k))) $ lines c
    conts <- readFile fname
    writeFile (fname ++ ".tmp") $ filtr conts
    renameFile (fname ++ ".tmp") fname

getEvent :: FilePath -> UTCTime -> IO (Maybe Event)
getEvent fname t = do
    conts <- readFile fname
    let xs = filter (\k -> t `timeIsIn` k) $ read <$> lines conts
    if xs == [] then return Nothing else return (Just (head xs))

getEvents :: FilePath -> IO [Event]
getEvents fname = do
    conts <- readFile fname
    return $ map read $ lines conts

getStartTime :: Event -> UTCTime
getStartTime = start . slot

getEndTime :: Event -> UTCTime
getEndTime = end . slot

timeIsIn :: UTCTime -> Event -> Bool
timeIsIn t e = (t >= getStartTime e) && (t <= getEndTime e)

newTime :: (Integer, Int, Int) -> (Int, Int, Int) -> Maybe UTCTime
newTime (y, m, d) (h, m', s) =
    let day = fromGregorianValid y m d
        time
            | h >= 24 = Nothing
            | m' >= 60  = Nothing
            | s >= 60  = Nothing
            | otherwise = Just (realToFrac (h*60*60 + m'*60 + s))
    in if isNothing day || isNothing time then Nothing
        else (Just (UTCTime (fromJust day) (fromJust time)))

markProductive :: FilePath -> UTCTime -> Trool -> String -> IO ()
markProductive fname time p msg  = do
    e <- getEvent fname time
    if isNothing e then return () else do
        let e' = updateProductive (fromJust e) p msg
        deleteEvent fname time
        writeEvent fname e'

updateProductive :: Event -> Trool -> String -> Event
updateProductive e p msg =
    e {productive = Productive {productive' = p, message = msg}}

sortEvents :: [Event] -> [Event]
sortEvents es =  sortBy f es
      where
        f e1 e2
            | e1' < e2' = LT
            | e1' > e2' = GT
            | otherwise = EQ
              where
                e1' = getStartTime e1
                e2' = getStartTime e2

