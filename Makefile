HFLAGS = -O0 -Wall -o ./bin/tt -odir ./lib/ -hidir ./lib/ -i./src/:./lib/ \
	-prof -fprof-auto -fprof-cafs

all: bindir libdir
	ghc $(HFLAGS) ./src/Main.hs

bindir:
	mkdir -p ./bin/

libdir:
	mkdir -p ./lib/

clean:
	rm -rf ./bin/
	rm src/*.o src/*.hi

